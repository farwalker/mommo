import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({

  state: {

    categories: {},
    products: {},

    popups: {
      categoryMenu: false
    }
  },
  mutations: {
    storeCategories (state, { categories }) {
      const catsDict = Object.fromEntries(categories.map(c => [ c._id.toString(), c ]))
      state.categories = { ...state.categories, ...catsDict }
    },
    storeProds (state, { prods }) {
      const prodsDict = Object.fromEntries(prods.map(p => [ p._id, p ]))
      state.products = { ...state.products, ...prodsDict }
    },
    showCategoryMenu (state) {
      state.popups.categoryMenu = true
    },
    hideCategoryMenu (state) {
      state.popups.categoryMenu = false
    }
  },
  actions: {},
  modules: {},

  getters: {}
})
