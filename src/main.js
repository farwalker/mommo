import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import './registerServiceWorker'
import { initStitch } from './api'

initStitch().then(stitch => {
  Vue.use(stitch)
  Vue.prototype.$stitch = stitch
  Vue.config.productionTip = false

  new Vue({

    router,
    store,
    render: h => h(App)

  }).$mount('#app')
})
