import Vue from 'vue'
import VueRouter from 'vue-router'
import { Page, Home, Listing, Cart } from '@/views'
import { moCategoryMenu } from '@/components/layout/header/topmenu'

Vue.use(VueRouter)

const routes = [ {
  path: '/',
  alias: '/home',
  name: 'home',
  component: Home,
  meta: {
    caption: 'Home'
  }
}, {
  path: '/category/:category?',
  name: 'listing',
  component: Listing,
  meta: {
    caption: 'Menu',
    dropdown: true,
    submenu: moCategoryMenu,
    after: {
      content: '▼'
    }
  }
}, {
  path: '/about',
  name: 'about',
  component: Page,
  meta: {
    caption: 'About'
  }
}, {
  path: '/contacts',
  name: 'contacts',
  component: Page,
  meta: {
    caption: 'Contacts'
  }
}, {
  path: '/cart',
  name: 'cart',
  component: Cart,
  meta: {
    caption: 'Order',
    after: {
      content: '►'
    }
  }
} ]

export default new VueRouter({ routes })
