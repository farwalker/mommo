async function getCategories ({ stitch: { client, db, user }, categoryId = null }) {
  const { auth: { isLoggedIn } } = client
  if (!isLoggedIn || !user || !db) {
    return false
  }
  const filter = {}
  if (categoryId) {
    filter.parent = categoryId
  }
  const result = await db.collection('categories').find(filter).asArray()
  return result
}

export default getCategories
