import {
  Stitch,
  RemoteMongoClient,
  AnonymousCredential
} from 'mongodb-stitch-browser-sdk'

const APP_KEY = 'mommocaffeeapp-xxjoj'
const SERVICE_NAME = 'ampulexClusterService'
const DB_NAME = 'MOMO_CAFFEE_DB'

async function initStitch () {
  const client = Stitch.initializeDefaultAppClient(APP_KEY)
  const db = client.getServiceClient(RemoteMongoClient.factory, SERVICE_NAME).db(DB_NAME)
  const user = await client.auth.loginWithCredential(new AnonymousCredential())
  return { client, db, user }
}

export default initStitch
