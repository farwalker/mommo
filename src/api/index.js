import getCategories from './getCategories'
import initStitch from './initStitch'

export { initStitch, getCategories }
