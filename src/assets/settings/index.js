export const MAX_PAGE_WIDTH = 1000
export const MAX_HEADER_HEIGHT = 240
export const MIN_HEADER_HEIGHT = 40
export const TOPMENU_BG = '#253545'
export const TOPMENU_BOTTOM = 40
