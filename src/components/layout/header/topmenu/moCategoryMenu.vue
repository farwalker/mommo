<template lang="pug">

  div.cat-menu.popup(:style="moCategoriesMenuStyles"
  v-show="shown" @mouseenter="handleMouseEnter"
  @mouseleave="handleMouseLeave" @click="handleClick"
  )
    mo-category-page(v-for="p in pages" :key="p.index" :page="p" @cattapped="onCattapped")

</template>

<script>

  import { MAX_PAGE_WIDTH } from '@/assets/settings'
  import { moCategoryPage } from './category'

  export default {

    components: { moCategoryPage },

    props: [ 'headerHeight' ],

    data () {
      return {
        hovered: false,
        pages: []
      }
    },

    name: "moCategoriesMenu",

    computed: {

      moCategoriesMenuStyles () {
        const { headerHeight } = this
        const top = 40 + (headerHeight < 60 ? 0 : headerHeight - 60) * 0.8
        return {
          left: `calc(50vw - 0.5 * ${MAX_PAGE_WIDTH}px + ${headerHeight}px - 20px)`,
          top: `calc(${top}px)`
        }
      },
      shown () {
        return this.$store.state.popups.categoryMenu
      },

      cats () {
        return this.$store.state.categories
      },
      currentCat () {
        const { $route: { query: { parent = null } } } = this
        return this.getCat(parent)
      }
    },
    created () {
      const items = this.getChildren(false)
      this.pages = [{ index: 0, items }]
      for (let index = 1; index < 10; index++) {
        this.pages.push({ index, items: [] })
      }
    },
    methods: {

      getCat (id) {
        if (!id) {
          return null
        }
        const { cats } = this
        if (id in cats) {
          return cats[id]
        } else {
          return null
        }
      },
      getChildren (id) {
        const { cats } = this
        const found = Object.values(cats).filter(c => (id && c.parent && c.parent.toString() === id.toString()) || (!id && !c.parent))
        console.log({ found })
        return found
      },
      getPath (id) {
        const path = []
        const cat = this.getCat(id)
        const _parent = c => {
          path.push(c._id)
          if (c.parent) {
            const parentCat = this.getCat(c.parent)
            _parent(parentCat)
          }
        }
        _parent(cat)
        path.push(false)
        path.reverse()
        return path
      },

      handleMouseEnter () {
        this.hovered = true
      },
      handleMouseLeave () {
        if (this.hovered) {
          setTimeout(() => {
            if (!this.hovered) {
              this.$store.commit("hideCategoryMenu")
            }
          }, 1000)
          this.hovered = false
        }
      },
      handleClick () {
        this.$store.commit("hideCategoryMenu")
      },
      onCattapped ({ cat, pageIndex }) {
        const children = this.getChildren(cat._id.toString())
        const { pages } = this
        const nextPage = pages[pageIndex + 1]
        if (nextPage) {
          nextPage.items = children
        }
        for (let i = pageIndex + 2; i < pages.length; i++) {
          pages[i].items = []
        }
      }
    }
  }

</script>

<style scoped lang="stylus">

  .cat-menu
    position fixed
    width auto
    height auto
    background-color rgb(57, 73, 89)
    display flex

</style>
