import moBurger from './moBurger'
import moFooter from './moFooter'
import moHeader from './moHeader'
import moMain from './moMain'

export { moMain, moHeader, moFooter, moBurger }
